![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

# GradGuide #

GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 

---

**Table of Contents**

- [Testing](#testing)
- [Again](#again)
- [Once More](#once-more)

## Testing

GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 


## Again

GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 


## Once More
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 
GradGuide takes the pain out of college advising and class registering by providing a streamlined workflow for college administrators, guidance counselors, and students. 
With GradGuide, students can easily view degree curriculums defined by college administrators. Students can also complete a ‘course checklist’ by which they select the 
classes they have completed, and a report is generated outlining the classes left to take which is automatically sent to a guidance counselor for verification. Students 
can then register themselves for class based on the recommendations. This process takes out the ambiguity of which courses are required for a degree, as all degree 
curriculums are laid out in the same format all in one place. Guidance counselors will be able to make more meaningful recommendations for classes to take, and students 
will be more informed as to which classes are required for their degree. 